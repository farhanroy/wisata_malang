package com.example.dummyapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.example.dummyapp.databinding.ActivityDetailGalleryBinding
import kotlinx.android.synthetic.main.activity_detail_gallery.*

class DetailGallery : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityDetailGalleryBinding>(this, R.layout.activity_detail_gallery).apply {
            val intent = intent.getParcelableExtra<Data>("data")
            val d:Data = Data(intent.caption, intent.thumbnail, intent.image)
            userModel = d
        }

        setSupportActionBar(toolbar)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener {
            startActivity(Intent(this@DetailGallery, MainActivity::class.java))
        }


    }
}
