package com.example.dummyapp

import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.dummyapp.adapter.GalleryAdapter
import com.example.dummyapp.viewmodel.GalleryViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: GalleryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_movie.setHasFixedSize(true)
        rv_movie.layoutManager = GridLayoutManager(this, 3)

        loading.visibility = View.VISIBLE   

        viewModel = ViewModelProviders.of(this).get(GalleryViewModel::class.java)

        viewModel.getGalleryAll().observe(this, Observer {
                t ->
            loading.visibility = View.GONE
            val adapter = t?.let { GalleryAdapter(it, this)}
            adapter?.notifyDataSetChanged()
            rv_movie.adapter = adapter
        })
        viewModel.onLoading.observe(this, Observer { it -> Log.d("Error", it) })


    }
}
