package com.example.dummyapp.Retrofit

import android.annotation.SuppressLint
import android.app.Application
import com.example.dummyapp.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@SuppressLint("Registered")
class ServiceRetrofit : Application(){
    private val client = OkHttpClient().newBuilder().addInterceptor(HttpLoggingInterceptor().apply {
        level = if(BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    })
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .build()
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://info-malang-batu.firebaseapp.com/")
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    val services: API = retrofit.create(API::class.java)
}