package com.example.dummyapp.Retrofit

import com.example.dummyapp.Data
import retrofit2.Call
import retrofit2.http.GET

interface API {
    @GET("Gallery_Malang_Batu.json?fbclid=IwAR1WHsl9ifLj0T63sFVdDx5d6YmAywyfyk_BzIu9D2ffe-6ZBjkm7c9Hu7c")
    fun getAllData(): Call <List<Data>>
}
