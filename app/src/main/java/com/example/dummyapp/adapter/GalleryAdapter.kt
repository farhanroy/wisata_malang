package com.example.dummyapp.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.dummyapp.DetailGallery
import com.example.dummyapp.Data
import com.example.dummyapp.R

import kotlinx.android.synthetic.main.item_row.view.*


class GalleryAdapter(private val list: List<Data>, val context: Context) : RecyclerView.Adapter<GalleryAdapter.Holder>(){

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_row,parent,false))
    }
    override fun getItemCount(): Int { return list.size }

    override fun onBindViewHolder(holder: Holder, i: Int) {

        Glide.with(context)
            .load(list[i].image)
            .centerCrop()
            .placeholder(R.drawable.ic_image_black_24dp)
            .error(R.drawable.ic_broken_image_black_24dp)
            .into(holder.view.img_poster)

        holder.view.img_poster.setOnClickListener {
            val intent = Intent(context, DetailGallery::class.java)
            val d: Data = Data(list[i].caption, list[i].thumbnail, list[i].image)
            println(list[i].caption)
            intent.putExtra("data",d)
            context.startActivity(intent)
        }

    }


    class Holder(val view: View) : RecyclerView.ViewHolder(view)

}