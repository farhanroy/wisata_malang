package com.example.dummyapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.dummyapp.Data
import com.example.dummyapp.Retrofit.ServiceRetrofit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GalleryViewModel : ViewModel(){
    val movieMutable = MutableLiveData<List<Data>>()
    val onLoading = MutableLiveData<String>()
    init {
        getGalleryAll()
    }

    fun getGalleryAll():MutableLiveData<List<Data>>{
        ServiceRetrofit().services.getAllData().enqueue(object : Callback <List<Data>> {
            override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                onLoading.value = t.message
            }

            override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                movieMutable.value = response.body()
            }

        })

        return movieMutable
    }

}