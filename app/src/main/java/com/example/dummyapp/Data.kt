package com.example.dummyapp

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.bumptech.glide.Glide
import android.widget.ImageView
import androidx.databinding.BindingAdapter


data class Data (
    @SerializedName("caption")
    var caption: String,
    @SerializedName("thumbnail")
    var thumbnail: String,
    @SerializedName("image")
    var image: String

):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(caption)
        parcel.writeString(thumbnail)
        parcel.writeString(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Data> {
        override fun createFromParcel(parcel: Parcel): Data {
            return Data(parcel)
        }

        override fun newArray(size: Int): Array<Data?> {
            return arrayOfNulls(size)
        }
    }


}

object ImageUtils{
    @JvmStatic @BindingAdapter("gallery")
    fun loadImage(imageView: ImageView, imageURL: String) {
        Glide.with(imageView.context)
            .load(imageURL)
            .centerCrop()
            .placeholder(R.drawable.ic_image_black_24dp)
            .error(R.drawable.ic_broken_image_black_24dp)
            .into(imageView)
    }
}